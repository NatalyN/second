<?php
/*Открываем сессию. Проверяем правильность введенного логина и пароля*/
include_once '../model/db_var.php';

 // создаем новую сессию или восстанавливаем текущую
session_start();
 // устанавливаем соединение с сервером БД
$conn = mysqli_connect($servername, $username, $password, $dbname);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}       

// уничтожаем данные авторизации
unset($_SESSION['login']);
unset($_SESSION['authorized']);

$_SESSION ['err'] = 0; 

if(isset($_POST['sub'])){
        $_POST['log']=  htmlspecialchars($_POST['log']);
        $_POST['pass']=  htmlspecialchars($_POST['pass']);
       // $sql = "SELECT log FROM auth WHERE log = '" . $_POST['log'] . "' AND pass='".$_POST['pass']."';";
        
        $sql = "SELECT pass FROM auth WHERE log = '" . $_POST['log'] . "';";
        
        $res=$conn->query($sql); // отправляем запрос к БД
        $n = $res->num_rows; // число строк в ответе на запрос
        if ($n!= 0){
            $row = $res->fetch_assoc (); 
            if (password_verify($_POST['pass'],$row['pass'])){           
                $_SESSION['login'] = $_POST['log'];// регистрируем переменную login 
                if($_POST['log']=='admin')
                    $_SESSION ['authorized'] = 1;
                else
                    $_SESSION ['authorized'] = 2;
                header("Location: ../controller/con_cabinet.php");//переход в личный кабинет
            }
            else $_SESSION ['err'] = 1;
        }
        else $_SESSION ['err'] = 1;
 }

 if (isset ( $_SESSION ['err'] ) && ($_SESSION ['err'] == 1)) {
    $_SESSION ['err'] = 0;
    include"../view/signin1.php";
}
$conn->close();
?>