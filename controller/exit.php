<?php
session_start();
unset($_SESSION['authorized']);
unset($_SESSION['login']);
unset($_SESSION ['err']);
session_destroy();
header("Location: ../view/index.php");
