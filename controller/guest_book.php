<?php session_start(); //Контроллер гостевой книги.

require "../model/guest_book.php"; // подключаем Модель

    // Обработка формы, если сценарий вызван через нее.
    if (!empty($_POST['doAdd']) ) {
	// Формируем массив (дата, логин, коммент) для последующей записи в бд. Сначала
	$new_comment = array(date('Y-m-d'), $_SESSION['login'] , $_POST['comment']);// новый комментарий хранится  в массиве
	// Записать книгу
	SaveBook($new_comment) ;
}
// Исполняемая часть сценария.
    $Book = LoadBook() ;
// Загружаем Шаблон страницы.
include "../view/review.php";
?>
