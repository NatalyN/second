<?php
class Hotel extends Room{
    protected $name="Берлога",$cat="***", $city="Харьков", $country="Украина";
    public $rooms=array();
    public function __construct($arr) {
        $this->rooms=$arr;
    }
    public static function add_booking($data1, $data2, $d, $n,$log){//добавляем новое бронирование-дата заезда,отъезда, кол-во ночей, массив из номеров комнат, пользователь
    include '../model/db_var.php';    
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");

        //добавляем бронирование в таблицу books
        //“Бронь” (поля “Номер брони”, “Дата заезда”, "Дата отъезда", "Номер комнаты", пользователь)
        $arr_num=array();
        foreach($n as $num){
            $tmp=$data1;
            $number=substr($data1,5,2).substr($data1,8,2).$num;//Номер бронирования состоит из месяц+дата+номер комнаты
            $arr_num[]=$number;
            $sql="INSERT INTO books(number, date_in, date_out, numr, login) "
                    . "VALUES"
                    . "( ".$number.",'".$data1."','".$data2."',".$num.",'".$log."');";
            $conn->query($sql);
            for($i=1; $i<=$d; $i++){
            //вносим на эти даты строки с номером бронирования, датами и номером комнаты в таблицу state
                $sql="INSERT INTO state (date_in, num, number) VALUES ( '".$tmp."',".$num.",".$number.") ;";
                if($conn->query($sql)){
                $tmp=date('Y-m-d',strtotime($tmp)+86400);}               
            }
        }
        $conn->close();
        return $arr_num;
    }
    public static function del_bron($v){//аргумент - массив бронирований для удаления

        include '../model/db_var.php';
        $v=implode("','",$v);
        $str="('".$v."')"; 
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        
        $sql="DELETE FROM books WHERE number IN ".$str.";";
        $conn->query($sql);
        
        $sql="DELETE FROM state WHERE number IN ".$str.";";//удаляем бронировки из таблицы состояний
        $conn->query($sql);
        
        $conn->close();
    }
    
    public static function pechat_bron($arr){//печать таблицы бронирований для выбора кандидата на удаление
        foreach ($arr as $val){
            echo"<tr><td><input type='checkbox' name='bron[".$val[0]."]'>"
            . "</td><td>".$val[0]."</td><td>".$val[1]."</td><td>".$val[2]."</td><td>".$val[3]."</td><td>".$val[4]."</td></tr>";
        }
    }
    public static function del_user($v){//аргумент - массив логинов для удаления

        include '../model/db_var.php';
        $v=implode("','",$v);
        $str="('".$v."')"; 
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        $sql="DELETE FROM auth WHERE log IN ".$str.";";//удаляем пользователя из таблицы auth
        $conn->query($sql);
        
        $sql="DELETE FROM guest WHERE log IN ".$str.";";//удаляем все каменты удаляемого пользователя
        $conn->query($sql);

        $sql="SELECT state.number as n FROM state INNER JOIN books WHERE books.number=state.number AND login IN ".$str.";";//находим все бронировки удаляемого пользователя из таблицы состояний
        $res=$conn->query($sql);
        if($res->num_rows>0){//если у него есть бронирования - отлавливаем их в массив для последующего удаления
            $arr=array();
            while($row=$res->fetch_assoc()){
                $arr[]=$row['n'];
            }
            $str1="(".implode(',',$arr).")";
            $sql="SELECT number FROM state WHERE number ".$str1.";";//удаляем все бронировки удаляемого пользователя из таблицы состояний
            $conn->query($sql);
        }
        
        $sql="DELETE FROM books WHERE login IN ".$str.";";//удаляем все бронировки удаляемого пользователя
        $conn->query($sql);
        $conn->close();
    }
    
    public static function pechat_user($arr){//печать таблицы пользователей для выбора кандидата на удаление
        foreach ($arr as $val){
            echo"<tr><td><input type='checkbox' name='user[".$val[0]."]'>"
            . "</td><td>".$val[0]."</td><td>".$val[1]."</td></tr>";
        }
    }
    public static function del_comment($v){//аргумент - массив каментов для удаления

        include '../model/db_var.php';
        $v=implode("','",$v);
        $str="('".$v."')"; 
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        $sql="DELETE FROM guest WHERE id IN ".$str.";";
        $conn->query($sql);
    }    
    public static function pechat_comment($arr){//печать таблицы пользователей для выбора кандидата на удаление
        foreach ($arr as $val){
            echo"<tr><td><input type='checkbox' name='comment[".$val[0]."]'>"
            . "</td><td>".$val[1]."</td><td>".$val[2]."</td><td class='lef'>".$val[3]."</td></tr>";
        }
    }
    public function pechat_cabinet(){
        foreach ($this->rooms as $val){
            echo"<tr>";
            for($i=0;$i<count($val);$i++)
                echo"<td>".$val[$i]."</td>";
            echo"</tr>";
        }
    }
    
    public function pechat(){//+колонка доступности номера сегодня
        foreach ($this->rooms as $val){
            $r = new Room($val->num,$val->category,$val->qty,$val->price1,$val->price2);
            if($r->state()) $res="Занят"; else $res="Свободен";
        echo"<tr><td class='link' onClick='show_room(".$val->num.")'>".$val->num."</td><td>".$val->category."</td><td>".$val->qty."</td><td>".$val->price1."</td><td>".$val->price2."</td><td>".$res."</td></tr>";
        }
    }
    public function pechat_book(){//колонка для бронирования
        foreach ($this->rooms as $val){
            $r = new Room($val->num,$val->category,$val->qty,$val->price1,$val->price2);
        echo"<tr><td><input type='checkbox' name='book[".$val->num."]'>"
            . "</td><td>".$val->num."</td><td>".$val->category."</td><td>".$val->qty."</td><td>".$val->price1."</td><td>".$val->price2."</td></tr>";
        }
    }
    
    public function pechat1(){
        foreach ($this->rooms as $val)
        echo"<tr><td>".$val->num."</td><td>".$val->category."</td><td>".$val->qty."</td><td>".$val->price1."</td><td>".$val->price2."</td></tr>";
    }
    public function sort_n(){
         function sortn($a,$b){
            if ($a->__get('num')>$b->__get('num')){ return 1;}
            elseif ($a->__get('num')<$b->__get('num')) {return -1;}
            else {return 0;}
        }
        uasort($this->rooms, "sortn"); 
    }
        public function sort_q(){
         function sortq($a,$b){
            if ($a->__get('qty')>$b->__get('qty')){ return 1;}
            elseif ($a->__get('qty')<$b->__get('qty')) {return -1;}
            else {return 0;}
        }
        uasort($this->rooms, "sortq"); 
    }
        public function sort_p(){
         function sortp($a,$b){
            if ($a->__get('price1')>$b->__get('price1')){ return 1;}
            elseif ($a->__get('price1')<$b->__get('price1')) {return -1;}
            else {return 0;}
        }
        uasort($this->rooms, "sortp"); 
    }
        public function sort_c(){
         function sortc($a,$b){
            if ($a->__get('category')>$b->__get('category')){ return 1;}
            elseif ($a->__get('category')<$b->__get('category')) {return -1;}
            else {return 0;}
        }
        uasort($this->rooms, "sortc"); 
    }    
}

