<?php //Модель гостевой книги.
// Загружает гостевую книгу из базы данных. Возвращает содержание книги.
function LoadBook() {
        include"../model/db_var.php";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        
        $sql="SELECT * FROM guest ORDER BY id DESC";
        $res=$conn->query($sql);
        $conn->close();
        if($res->num_rows ==0)
            return array();
        else{
            $Book=array();
            
            while($row=$res->fetch_assoc()){
                $book1 = array($row['data'], $row['log'], $row['comment']);
                $Book[]=$book1;
            }
            
        }     
        return $Book;
}
// Сохраняет содержимое нового коммента в БД
function SaveBook($book) {
    include"../model/db_var.php";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        
        $sql="INSERT INTO guest (data, log, comment) VALUES ('".$book[0]."', '".$book[1]."', '".$book[2]."')";
        $res=$conn->query($sql);
        $conn->close();
}