<?php
function autoload1($clsname){
	$filename='../model/'.$clsname.".php";
	if (is_readable($filename))
		require $filename;
}

function autoload2($clsname){
	$filename="../htdocs/Class/".$clsname.".php";
	if(is_readable($filename))
		require $filename;
}
spl_autoload_register("autoload1");
spl_autoload_register("autoload2");