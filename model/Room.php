<?php

class Room{
    public $qty;//вместимость номера
    public $category, $price1, $price2;//категория, стандартная цена, цена в высокий сезон
    public $num;//номер комнаты
    
    public function  __construct($num, $cat,$qty,  $pr1, $pr2){
            $this->qty=$qty;
            $this->category=$cat;
            $this->price1=$pr1;
            $this->price2=$pr2;
            $this->num=$num;
    }
        
    protected function state(){//свободен/занят на текущую дату
        $today=date('Y-n-d');;
        
    include '../model/db_var.php';
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        $sql="SELECT * FROM state WHERE date_in='".$today."' AND num=".$this->num.";";
        $res=$conn->query($sql);
        if ($res->num_rows>0)return true;
        else return false;
    }
    public function __get($name){
        switch($name){
            case 'num': return $this->num;
            case 'qty': return $this->qty;
            case 'price1': return $this->price1;
            case 'category': return $this->category;
            case 'state': return $this->state();
        }

    }
}
