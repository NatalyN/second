<?php
include "autoload.php";
include_once '../model/db_var.php';
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        
        $arr=array();
        $sql="SELECT num,category,quantity, price1,price2 FROM rooms;";
        $res=$conn->query($sql);
        

        
        if ($res->num_rows > 0) {	//если база номеров не пуста	
            while ( $row = $res->fetch_assoc () ) {
		$arr1=new Room($row ["num"] , $row ["category"] , $row ["quantity"] , $row ["price1"], $row ["price2"]);
                $arr[]=$arr1;
            }
            $g=new Hotel($arr);
            $err=0;

            
            if(isset($_POST['sub1'])){//обработка чекбоксов
                $arr1=$arr2=$arr3=array();
               
                if(isset($_POST['vybor'][1])||isset($_POST['vybor'][2])||isset($_POST['vybor'][3])){//выбрана категория номера
                    foreach($g->rooms as $room){
                            for($i=1;$i<4;$i++)
                                if(isset($_POST['vybor'][$i])&&($_POST['vybor'][$i])==$room->__get('category')){
                                    $arr1[]=$room;
                                }
                    }
                    if (count($arr1)>0)
                        $g= new Hotel($arr1);
                    else $err=1;
                        
                }
                if(isset($_POST['vybor'][4])||isset($_POST['vybor'][5])){//выбрана вместительность номера    
                    foreach($g->rooms as $room){
                        for($i=4;$i<6;$i++)    
                            if(isset($_POST['vybor'][$i])&&($_POST['vybor'][$i])==$room->__get('qty')){
                                $arr2[]=$room;
                            }
                    }
                    if (count($arr2)>0)
                        $g= new Hotel($arr2);
                    else $err=1;
                }
                if(isset($_POST['ot'])||isset($_POST['do'])){//выбрана цена номера 
                    
                    foreach($g->rooms as $room){
                        if(isset($_POST['ot']))
                            if($_POST['ot']<=$room->__get('price1'))
                                if(isset($_POST['do'])){
                                    if($_POST['do']>=$room->__get('price1'))
                                        $arr3[]=$room;}
                                else $arr3[]=$room;//До не установлено, От установлено и подходит                            
                    }
                    if (count($arr3)>0)
                        $g= new Hotel($arr3);
                    else $err=1;
                }//конец обработки цены                
            }//конец обработки блока чекбоксов
            
                        if(isset($_POST['sort'])){//если нажата кнопка "Упорядочить по..."
                switch ($_POST['sort']){
                    case "guest": {$g->sort_q();break;}
                    case "price": {$g->sort_p();break;}
                    case "category": {$g->sort_c();break;}
                    default : $g->sort_n();
                }
            }
            else $g->sort_n();
            
            if($err==1) 
                echo"<table><tr><th><h1>По Вашему запросу ничего не найдено<h1></th></td></table>";
            else $g->pechat();
        }
        else "<table><tr><th>Этот отель пока не достроен :)</th></td></table>";