<?php
include "autoload.php";
include_once '../model/db_var.php';
        $conn = new mysqli($servername, $username, $password, $dbname);
        if($conn->connect_error) die ("Can't connect to database.");
        
        $arr=array();
        $arr2=array();
        $sql="SELECT num,category,quantity, price1,price2 FROM rooms;";
        $res=$conn->query($sql); 
       
        if ($res->num_rows > 0) {	//если база номеров не пуста	
            while ( $row = $res->fetch_assoc () ) {
		$arr1=new Room($row ["num"] , $row ["category"] , $row ["quantity"] , $row ["price1"], $row ["price2"]);
                $arr[]=$arr1;//массив комнат
                $arr2[]=$row['num'];//массив номеров комнат
            }
            if (!isset($_SESSION['authorized']))
                echo"<tr><th colspan='6' class='attention'>Для оформления бронирования необходимо войти в свой аккаунт.</th></tr>";
            else {
                if(isset($_GET['sub'])){
                    $_SESSION['in']=$_GET['in'];
                    $_SESSION['out']=$_GET['out'];
                }
            }
            if(isset($_GET['sub'])){//нажата кнопка Искать свободные номера на даты
                //$arr=array();
                $arr1=array();
                $arr=$arr2;
                $d=round((strtotime($_GET['out'])-strtotime($_GET['in']))/86400);  //На сколько дней нужен номер       
                $data=$_GET['in'];//дата заезда
                for ($i=1;$i<=$d;$i++){                 
                    $str='('.implode(',',$arr).')';//массив номеров свободных комнат
                    //ищем занятые на дату комнаты
                    $sql="SELECT DISTINCT num FROM state WHERE date_in='".$data."' AND num IN".$str.";"; 
                    $res=$conn->query($sql);
                    if ($res->num_rows > 0) {	//если нашлись занятые номера на этот день (из свободных в предыдущие)	
                        $arr1=array();
                        while ( $row = $res->fetch_assoc () ) {                                   
                            $arr1[]=$row ["num"];
                        }
                        $arr=array_diff($arr,$arr1);// вычеркиваем их из общего списка
                    }
                    if (count($arr>0))//массив свободных номеров еще не пуст
                        $data=date('Y-m-d',strtotime($_GET['in'])+86400*($i));//увеличиваем дату заезда на 1
                    else  break;
                }
                    
                if (count($arr)>0){//создаем массив из свободных на заданные даты комнат
                    $str='('.implode(',',$arr).')';
                    $arr=array();
                    $sql="SELECT num,category,quantity, price1,price2 FROM rooms WHERE num IN".$str.";";
                    $res=$conn->query($sql);
                    if ($res->num_rows > 0) {//если нашлись свободные номера на даты	
                        while ( $row = $res->fetch_assoc () ) {                                   
                            $arr1=new Room($row ["num"] , $row ["category"] , $row ["quantity"] , $row ["price1"], $row ["price2"]);
                            $arr[]=$arr1;
                        }
                        echo"<tr><th colspan='6'>Свободные номера на ".$_GET['in']." -- ".$_GET['out'].":</th></tr>";
                    }                   
                    
                } // далее - если нет свободных номеров                      
                if (count($arr)==0)    
                    echo"<tr><th colspan='6' class='attention'>Извините, нет свободных номеров на выбранные даты.</th></tr>";
            } 
            $g=new Hotel($arr); //создаем объект из массива комнат
        
            if(isset($_POST['book'])&&isset($_SESSION['authorized'])&&isset($_SESSION['out'])){//выбраны номера для брони
                $v=array_keys($_POST['book']);//массив выбранных для брони номеров комнат
                    $d=round((strtotime($_SESSION['out'])-strtotime($_SESSION['in']))/86400); //кол-во дней брони
                    $bron=Hotel::add_booking($_SESSION['in'],$_SESSION['out'],$d, $v, $_SESSION['login']); 
                    echo"<tr><th colspan='6' class='attention'>БРОНИРОВАНИЕ УСПЕШНО ОФОРМЛЕНО.<br>";
                    for($i=0;$i<count($bron);$i++){
                        echo "№".$bron[$i]."; ";
                    }
                    echo " Последние 3 цифры - номер комнаты </th></tr>";
            }
            
            if (isset($_SESSION['authorized'])&&(!isset($_GET['sub']))){
                unset($_SESSION['in']);
                unset($_SESSION['out']);
            }
            $g->pechat_book();
            if(isset($_POST['book'])&&(!isset($_GET['out']))){//попытка бронирования без регистрации, или без выбора даты
                if (isset($_SESSION['authorized'])){
                    if(!isset($bron))
                        echo"<table><tr><th>ПОЖАЛУЙСТА, НАЙДИТЕ СВОБОДНЫЕ НОМЕРА НА НУЖНЫЕ ДАТЫ.</th></tr></table>";
                }
                else
                    echo"<table><tr><th>БРОНИРОВАНИЕ ДОСТУПНО ТОЛЬКО ЗАРЕГЕСТРИРОВАННЫМ ПОЛЬЗОВАТЕЛЯМ.</th></tr></table>";
            }    
            if(isset($_POST['sub_book'])&&(!isset($_POST['book'])))
                echo"<table><tr><th>ВЫ НЕ ВЫБРАЛИ НОМЕР.</th></tr></table>";
        }
        
        else 
            echo"<table><tr><th>Этот отель пока не достроен :)</th></tr></table>";
        
$conn->close();

