<?php session_start();?>
<!DOCTYPE>
<!--Страница регистрации-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Регистрация</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css"/>
    <script type="text/javascript" src="../JS/script.js"></script>
</head>
<body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";//                                 </td></tr></table>
        include_once "left.html";?>
    
    <div id="content">
        <br/><br/>
        <table class="small">
            <tr><td>
            <div class="forms">
                <form name="my" method="GET" action="../model/reg.php" onSubmit="return check_reg(this);">            
                    <h2><label for='my'>Пожалуйста, введите Ваши данные:</label><br/></h2>
                     <div class="field">
                        <label for="log">Логин:</label><span>*</span>
                        <input type="text" name="log" required="" autofocus=""/><br/><br/>
                    </div>
                    
                    <div class="field">
                        <label for="mail">E-mail:</label><span>*</span>
                        <input type="email" name="mail" required=""/><br/><br/>
                    </div>
                        
                    <div class="field">
                        <label for="pass">Пароль(не менее 4 символов):</label><span>*</span>
                        <input type="password" name="pass" required=""><br/><br/>
                    </div>
                        
                    <div class="field">
                        <label for="conf">Подтвердите пароль:</label><span>*</span>
                        <input type="password" name="conf" required=""/><br/><br/>
                    </div>
                        <button type="submit" name="sub">ОК</button>
                </form>
            </div>            
        </td></tr>
            <?php if(isset($err))
                    echo"<tr><th class='attention'>". $err."</th></tr>";?>
        </table>                    
    </div>

</body>
</html>
