<!DOCTYPE html>
<!--Админка, удаление пользователя-->
<html>
    <head>
        <title>Админка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>

        <div id="content">       
            
            <div>
                <br/>
                <table>
                    <tr><th colspan="6">
                        <form name="admin" action="../controller/delete.php" method="POST">
                            <label for="admin">Удалить:</label>
                            <button type="submit" name="user">Пользователя</button>
                            <button type="submit" name="comment" >Комментарий</button>
                            <button type="submit" name="bron" disabled="">Бронирование</button>
                        </form>
                    </th></tr>
                </table>
                    <br/><br/>
                <table>
                    <tr><th colspan="6">Список бронирований</th></tr>
                    <form action='../controller/delete.php' method='POST' name='bron'><!--чекбоксы выбора пользователей для удаления-->
                    <tr><th><button type='submit' name='del'>OK</button></th><th>№</th><th>Дата заезда</th><th>Дата отъзда</th><th>№ комнаты</th><th>Пользователь</th></tr>
                    <?php
                        include '../model/del_bron.php';
                    ?>
                    </form>
                </table>
            </div>
        </div>
    </body>
</html>




<?php



