<!DOCTYPE html>
<!--Страница авторизации-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Вход</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css"/>
    <script type="text/javascript" src="../JS/script.js"></script>
</head>

<body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
    <div id="content">
            <br/><br/>
        <div id="forms">
            <table class="small"><tr><th>Неверный ввод! Попробуйте еще раз</th></tr><tr>
               <td>
            <div class="forms">                         
            <form method="post" action="../controller/con_signin.php" name="myform" OnSubmit="return check_log_pass(this)">
                <h2><label for='myform'> Войдите в систему: </label><br/></h2>
                <div class="field"> 
                    <label for="log">Логин</label>
                    <input type="text" name="log" required="" autofocus=""/><br/><br/>
                </div>
                
                <div class="field">
                    <label for="pass">Пароль</label>
                    <input type="password" name="pass" required=""/><br/><br/><br/>   
                </div>
                <button type="submit" name="sub">Вход</button>
          </form>                       
        </div> 
                </td></tr></table>
        </div>
    </div>
</body>
</html>


