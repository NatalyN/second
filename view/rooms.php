<?php session_start();?>
<!DOCTYPE html>
<!--Список номеров с характеристиками, доступно всем читателям-->
<html>
    <head>
        <title>Номера</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
        <script type="text/javascript" src="../JS/script.js"></script>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>

        <div id="content">
       
            <div>
                <br/>
                <table>
                    <tr><th colspan="2">
                    <form name="mysort" action="../view/rooms.php" method="POST">
                        <label for="mysort">Упорядочить по:</label>
                        <select name="sort" >  
                            <?php 
                                $arr1=array('num', 'guest', 'price', 'category');
                                $arr2=array('Номеру комнаты', 'Макс. кол-ву гостей', 'Цене', 'Категории');
                                for($i=0; $i<4;$i++){
                                    if(isset($_POST['sort'])){//если нажата кнопка "Упорядочить по..."
                                        $selected=($_POST['sort']==$arr1[$i])?'selected="select"':null;
                                    }
                                    else $selected=null;
                                    echo"<option value='".$arr1[$i]."' ".$selected."'>".$arr2[$i]."</option> ";
                                }
                                    
                            ?><!--
                            <option value="num">Номеру комнаты</option>
                            <option value="guest">Макс. кол-ву гостей</option>
                            <option value="price">Цене</option>
                            <option value="category">Категории</option> -->
                        </select>
                        <button type="submit" name="sub">OK</button>        
                    </form>
                    
                </th><td colspan="4" >
                    <form action="../view/rooms.php" method="POST" name="search" >
                        <table class="check"><tr><th>
                                Категория номера:<br/>
                                Макс. кол-во гостей:<br/>
                                Цена:
                                </th><td>
                                    <input type="checkbox" name="vybor[1]" value="Люкс"/>Люкс
                                    <input type="checkbox" name="vybor[2]" value="Стандарт"/>Стандарт
                                    <input type="checkbox" name="vybor[3]" value="Эконом"/>Эконом<br/>
                        
                                    <input type="checkbox" name="vybor[4]" value="2"/>2
                                    <input type="checkbox" name="vybor[5]" value="3"/>3<br/>
                                    <input type="number" name="ot" min="200" max="800" value="200"/>до
                                    <input type="number" name="do" min="200" max="800" value="800"/>  
                                </td><td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="submit" name="sub1">Искать</button><br/>
                        </td></tr></table>   
                    </form> 
                        </td></tr>
                    <tr><th colspan="6"><h1>Наши аппартаменты</h1></th></tr>
                    <tr><th>Номер комнаты</th><th>Категория номера</th><th>Макс. кол-во гостей</th><th>Цена за номер, грн.</th><th>Цена в праздничные дни</th><th>Доступность на сегодня</th></tr>
                    <?php
                        include '../model/show_rooms.php';
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>

