<!DOCTYPE html>
<!--Контактная инфа, карта проезда-->
<html>
    <head>
        <title>Личный кабинет</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
        <div id="content">
       
            <div>
                <br/>
                <table>
                    <tr><th colspan="6"><h1>Ваши бронирования</h1></th></tr>
                    <tr><th>№ брони</th><th>Номер комнаты</th><th>Категория номера</th><th>Макс. кол-во гостей</th><th>Дата заезда</th><th>Дата отъезда</th></tr>
                    <?php
                        include '../model/show_bron.php';
                        echo $mes;
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>

