
<!DOCTYPE html>
<!--Контактная инфа, карта проезда-->
<html>
    <head>
        <title>Контакты</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
        
        <div id="content">
            <br/>
            <table><tr><th colspan="2">
                <h1>Добро пожаловать в "Берлогу"</h1>
            </th></tr>
            
            <tr><td>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d137121.72234033974!2d19.0049134!3d74.42501359999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x45bdf181870a1d07%3A0x927c85745dee2378!2z0JzQtdC00LLQtdC20LjQuSwgQmrDuHJuw7h5YSA5MTc2LCDQqNC_0LjRhtCx0LXRgNCz0LXQvQ!5e0!3m2!1sru!2sua!4v1423499781704" width="400" height="300" frameborder="0" style="border:0"></iframe>
            </td>            
             <th>                
                <form method="post" action="../controller/con_contact.php" name="myform" id="mail">
                    <p> Форма обратной связи</p>
                    <h3><?php if(isset($mes)) echo $mes;?></h3>
            <div class="forms"> 
                    <div class="field">
                        <label for="subj">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Тема</label><span> * </span>
                        <input type="text" name="subj" size="40" required=""/>
                    </div>
                    
                    <div class="field">
                        <label for="subj">Сообщение</label><span> * </span>
                        <textarea rows="15" cols="30" name="mes" required=""></textarea>
                    </div>
                    <p><button type="reset" name="sub" width="40">&nbsp;Очистить&nbsp;&nbsp;</button></p>    
                    <p><button type="submit" name="sub" size="60">Отправить</button></p>
                </form>
            </div>    
            </th></tr>
        </table>
        </div>
    </body>
</html>


