<?php session_start();?>
<!DOCTYPE html>
<!--Главная страница отеля-->
<html>
    <head>
        <title>Главная</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
        
        <div id="content">
            <br/>
            <table class="main"><tr><th>
                <h1>Добро пожаловать в "Берлогу"</h1>
            </th></tr></table>
            <p>
                <img class="main" src="../images/hotel.jpg" alt="berloga"/>
            </p>
        </div>
    </body>
</html>
