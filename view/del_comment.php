<!DOCTYPE html>
<!--Админка, удаление пользователя-->
<html>
    <head>
        <title>Админка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>

        <div id="content">       
            
            <div>
                <br/>
                <table>
                    <tr><th colspan="6">
                        <form name="admin" action="../controller/delete.php" method="POST">
                            <label for="admin">Удалить:</label>
                            <button type="submit" name="user">Пользователя</button>
                            <button type="submit" name="comment" disabled="">Комментарий</button>
                            <button type="submit" name="bron">Бронирование</button>
                        </form>
                    </th></tr>
                </table>
                    <br/><br/>
                <table>
                    <tr><th colspan="4">Список пользовательских комментариев</th></tr>
                    <form action='../controller/delete.php' method='POST' name='comment'><!--чекбоксы выбора пользователей для удаления-->
                    <tr><th><button type='submit' name='del'>OK</button></th><th>Дата</th><th>Пользователь</th><th>Комментарий</th></tr>
                    <?php
                        include '../model/del_comment.php';
                    ?>
                    </form>
                </table>
            </div>
        </div>
    </body>
</html>




<?php

