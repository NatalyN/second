<!DOCTYPE>
<!--Страница регистрации-->
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Отзывы</title>
    <link rel="stylesheet" type="text/css" href="../css/main.css"/>
    <script type="text/javascript" src="../JS/script.js"></script>
</head>
<body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
    
    <div id="content">
            <br/>
                <table><tr><td>
                <?php  
                    if(isset($_SESSION['authorized']))
                        include "add_comment.html";
                    else
                        echo "<h3>Комментарии могут оставлять только зарегистрированные пользователи</h3>"?>
                
                <h2>Гостевая книга:</h2>
                <?php foreach ($Book as $id=>$v) {
                    echo "<p class='book'>".$v[0]." 
                    Имя: ".$v[1]."<br/> 
                    Комментарий:<br/>
                    ".nl2br($v[2])."</p><hr/> ";
                 }?> 
                    
                </td></tr></table>
            </div>
    </div>
</body>
</html>


