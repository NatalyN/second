<!DOCTYPE html>
<!--Админка, удаление пользователя-->
<html>
    <head>
        <title>Админка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>

        <div id="content">       
            
            <div>
                <br/>
                <table>
                    <tr><th colspan="6">
                        <form name="admin" action="../controller/delete.php" method="POST">
                            <label for="admin">Удалить:</label>
                            <button type="submit" name="user" disabled="" >Пользователя</button>
                            <button type="submit" name="comment">Комментарий</button>
                            <button type="submit" name="bron">Бронирование</button>
                        </form>
                    </th></tr>
                </table>
                    <br/><br/>
                <table>
                    <tr><th colspan="3">Список пользователей</th></tr>
                    <form action='../controller/delete.php' method='POST' name='user'><!--чекбоксы выбора пользователей для удаления-->
                    <tr><th><button type='submit' name='del'>OK</button></th><th>Логин</th><th>E-mail</th></tr>
                    <?php
                        include '../model/del_user.php';
                    ?>
                    </form>
                </table>
            </div>
        </div>
    </body>
</html>




