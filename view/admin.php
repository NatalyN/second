<!DOCTYPE html>
<!--Личный кабинет админа-->
<html>
    <head>
        <title>Админка</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>

        <div id="content">       
            
            <div>
                <br/>
                <table>
                    <tr><th colspan="6">
                        <form name="admin" action="../controller/delete.php" method="POST">
                            <label for="admin">Удалить:</label>
                            <button type="submit" name="user">Пользователя</button> 
                            <button type="submit" name="comment">Комментарий</button>
                            <button type="submit" name="bron">Бронирование</button>
                        </form>
                    </th></tr>
                </table>
                    <br/><br/>
                <table>
                    <tr><th colspan="6">ВАШИ БРОНИРОВАНИЯ</th></tr>
                    <tr><th>№ брони</th><th>Номер комнаты</th><th>Категория номера</th><th>Макс. кол-во гостей</th><th>Дата заезда</th><th>Дата отъезда</th></tr>
                    <?php
                        include '../model/show_bron.php';
                    ?>
                </table>
            </div>
        </div>
    </body>
</html>



