<?php session_start();?>
<!DOCTYPE html>
<!--Поиск свободных на выбранные даты номеров.Доступно всем-->
<html>
    <head>
        <title>Бронирование</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="../css/main.css" type="text/css"/>
        <script type="text/javascript" src="../JS/script.js"></script>
    </head>
    <body>
    <?php 
        $name=basename(__FILE__);
        include_once "top.php";
        include_once "left.html";?>
        <div id="content">
       
            <div>
                <br/>
                <table>
                    <tr><th colspan="6"><h3>Введите предполагаемые даты заезда и отъезда</h3></th></tr>
                    <tr><th colspan="3">
                    <form name="mysort" action="../view/book.php" method="GET" OnSubmit="return checkData(this)">
                        Прибытие:
                        <input type="date" name="in" placeholder="YYYY-mm-dd" value=""/><!--<//?php echo date('d.m.Y');?>"/>  -->
                    </th><th colspan="3" >
                        Отъезд:
                        <input type="date" name="out" placeholder="YYYY-mm-dd" value=""/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <button type="submit" name="sub" >Искать</button>
                    </form>
                        </th></tr>
                    <tr><th colspan="6"><h1>Наши аппартаменты</h1></th></tr>
                <form action='../view/book.php' method='POST' name='book'><!--чекбоксы выбора номера для брони-->
                    <tr><td><button type='submit' name='sub_book'>OK</button></td><th>Номер комнаты</th><th>Категория номера</th><th>Макс. кол-во гостей</th><th>Цена за номер, грн.</th><th>Цена в праздничные дни</th></tr>
                    <?php
                        include '../model/booking.php';
                    ?>
                </form>
                </table>
            </div>
        </div>
    </body>
</html>


