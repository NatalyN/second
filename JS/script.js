function checkData(f){// проверяем ввведенны даты на соответствие формату YYY-mm-dd. Заезд - больше текущей даты. 
    ////Отъезд - больше заезда, но меньше, чем на 2 месяца вперед
    var D = new Date();
    D.setMonth(D.getMonth() + 2);

    if (new Date(f.in.value)=='Invalid Date'||new Date(f.in.value)<new Date(new Date()- 79199998)){
        alert("Введите правильную дату прибытия");
        //f.in.value=focus();
        return false;
    }
    else if (new Date(f.out.value)=='Invalid Date'){
        alert("Введите правильную дату отъезда");
        //f.out.value=focus();
        return false;
    }
    
    else if ((Date.parse(f.out.value)-Date.parse(f.in.value))<3600*24*1000){
        alert("Дата отъезда должна быть больше даты заезда");
        //f.out.value=focus();
        return false;
    }
    else if (new Date(f.out.value)>new Date(D)){
        alert("Извините, бронирование на выбранный месяц пока не доступно");
        //f.out.value=focus();
        return false;
    }
    else 
        return true;
}
function check_log_pass(f){//проверка того, что логин не включают пробельные символы и др. не дозволенные
    var reg=/\W+|\s+/;
    if(reg.test(f.log.value)||reg.test(f.pass.value)) {
        alert("Логин и пароль могут содержать только латинские буквы, цифры и символ подчеркивания");
        return false;  
    }
    else
        return true;
}
function check_reg(f){//проверка данных, введенных для регистрации нового пользователя
    var reg=/\W+|\s+/;//проверка того, что логин не включают пробельные символы и др. не дозволенные
    var reg_e=/^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
    if((f.log.value).length>10){
        alert("Логин не должен превышать 10 символов");
        return false;
    }
    else if(reg.test(f.log.value)) {
        alert("Логин может содержать только латинские буквы, цифры и символ подчеркивания");
        //f.log.value=focus();
        return false;  
    }
    else if(!(reg_e.test(f.mail.value))) {
        alert("Введите действующий e-mail");
        //f.mail.value=focus();
        return false;  
    }
    else if(reg.test(f.pass.value)) {
        alert("Пароль может содержать только латинские буквы, цифры и символ подчеркивания");
        f.pass.value=focus();
        return false;  
    }
    else if((f.pass.value).length<4) {
        alert("Пароль должен быть не короче 4 символов");
        f.pass.value=focus();
        return false; 
    }
    else if((f.pass.value).length>20) {
        alert("Длина пароля ограничена 20-ю символами");
        f.pass.value=focus();
        return false; 
    }
    else if(f.pass.value!= f.conf.value) {
        alert("Пароли должны совпадать");
        return false; 
    }
    else
        return true;
}
function check_length(f){
    if((f.comment.value).length>255){
        alert("Сообщение длиннее 255 символов.");
        //f.comment.value=focus();
        return false;
    }
    else 
        return true;
}
function check_ajax(){
    if(window.XMLHttpRequest){
        xmlHttp= new XMLHttpRequest;
    }
    else{
        xmlHttp=false;
        try{
            xmlHttp= new ActiveXObject("Msxml2.XMLHTTP");
        }
        catch (e){
            try{
                xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
            }        
            catch(e){}
        }
    }
    xmlHttp.onReadyStateChange=function(){
        if (xmlHttp.readyState == 4) {
            if (xmlHttp.status == 200) {
                document.getElementById("dest").innerHTML = xmlHttp.responseText;
            }
            else {
                document.getElementById("dest").innerHTML = "Код ошибки: " + xmlHttp.status + " " + xmlHttp.statusText;
            }
        }
    };
        city = document.getElementById("city").value;
        var url = "process.php?city=" + city;
        xmlHttp.open("GET", url, true);
        xmlHttp.send(null);
}
function show_room(number){
    window.open("../view/room"+number+".html","+number+","height=450,width=600");
}